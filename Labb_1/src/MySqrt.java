
public class MySqrt {

    /**
     * Performs a binary search to find a value whos square is within epsilon of x
     * @param x the number whos root will be returned
     * @param epsilon acceptable error
     * @return an approximate root of x
     */
    public static double mySqrtLoop(double x, double epsilon) {
        double max, min, root;

        // Validate input
        if (x < 0) throw new IllegalArgumentException();

        //Check which limits to start with
        if (x <= 1) {
            max = 1;
            min = x;
        } else {
            max = x;
            min = 1;
        }

        //Until we find a value whos square is less than 10e-6 from x
        //perform a binary search for such a value.
        while (true) {
            //Candidate root
            root = min + (max - min)/2;
            //Candidate square
            double square = root * root;
            //Return if candidate is good enough
            if (Math.abs(x - square) <= epsilon) {
                return root;
            }

            //If root wasn't good enough, adjust limits
            if (square < x) {
                min = root;
            } else {
                max = root;
            }
        }
    }

    /**
     * Performs a binary search to find a value whos square is within epsilon of x
     * @param x the number whos root will be returned
     * @param epsilon acceptable error
     * @return an approximate root of x
     */
    public static double mySqrtRecurse(double x, double epsilon) {

        //Validate input
        if (x < 0) {
            throw new IllegalArgumentException();
        }

        //Pick limits
        if (x <= 1) {
            return binarySearch(x, epsilon, x,1);
        } else {
            return binarySearch(x, epsilon, 1, x);
        }
    }

    /**
     * Performs a binary search to find a value whos square is within epsilon of x
     * @param x the number whos rootwill be returned
     * @param epsilon acceptable error
     * @param min least posible value of root
     * @param max largest possible value of root
     * @return an approximate root of x
     */
    private static double binarySearch(double x, double epsilon, double min, double max) {
        double root = min + (max - min)/2;
        double square = root * root;
        if (Math.abs(x - square) <= epsilon) {
            return root;
        }
        if (square < x) {
            return binarySearch(x, epsilon, root, max);
        }
        return binarySearch(x, epsilon, min, root);
    }

    public static void main(String [] args) {
        final double EPS = 1e-6;

        //Tests for the above algorithms.

        //ITERATIVE
        System.out.println("\nITERATIVE\n-------");
        try {
            mySqrtLoop(-1, EPS);
            throw new Error();
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid input, x negative");
        }
        System.out.println(mySqrtLoop(0, EPS) + ": Should be close to 0");
        System.out.println(mySqrtLoop(0.4, EPS) + ": should be close to 0.634255");
        System.out.println(mySqrtLoop(1, EPS) + ": should be close to 1");
        System.out.println(mySqrtLoop(16, EPS) + ": should be close to 4");
        System.out.println(mySqrtLoop(Integer.MAX_VALUE, EPS) + ": should be close to 46340.95");

        //RECURSIVE
        System.out.println("\nRECURSIVE\n-------");
        try {
            mySqrtRecurse(-1, EPS);
            throw new Error();
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid input, x negative");
        }
        System.out.println(mySqrtRecurse(0, EPS) + ": Should be close to 0");
        System.out.println(mySqrtRecurse(0.4, EPS) + ": should be close to 0.634255");
        System.out.println(mySqrtRecurse(1, EPS) + ": should be close to 1");
        System.out.println(mySqrtRecurse(16, EPS) + ": should be close to 4");
        System.out.println(mySqrtRecurse(Integer.MAX_VALUE, EPS) + ": should be close to 46340.95");
    }

}
