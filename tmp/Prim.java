import java.util.*;

public class Prim {
    public static void main(String[] args) {
        System.out.println(new Prim().prims());
        System.out.println(new Prim().kruskals());
    }

    List<List<Edge>> graph;
    private final int NUM_NODES = 6;

    public Prim() {
        graph = new ArrayList<>();
        for (int i = 0; i < NUM_NODES; i++) {
            graph.add(new LinkedList<>());
        }
        addEdge(0,1,5.0);
        addEdge(0,2,8.0);
        addEdge(1,2,5.0);
        addEdge(1,3,2.0);
        addEdge(2,3,4.0);
        addEdge(2,4,7.0);
        addEdge(3,4,5);
        addEdge(3,5,6.0);
        addEdge(4,5,3.0);
    }


    public List<Edge> kruskals() {
        List<Edge>[] components = (List<Edge>[]) new List[NUM_NODES];
        for (int i = 0; i < NUM_NODES; i++) {
            components[i] = new LinkedList<>();
        }
        Queue<Edge> pq = new PriorityQueue<>();
        for (List<Edge> l : graph)
            for (Edge e : l)
                pq.add(e);       
        while (!pq.isEmpty()) {
            Edge e = pq.poll();
            if (components[e.from] != components[e.to]) {
                components[e.from].add(e);
                components[e.from].addAll(components[e.to]);
                for (Edge edge : components[e.to]) {
                    components[edge.from] = components[e.from];
                    components[edge.to] = components[e.from];
                }
                components[e.to] = components[e.from]; //Handle case of empty to-list
            }
        }
        return components[0];
    }

    public List<Edge> prims() {
        int startNode = 0;
        List<Edge> tree = new LinkedList<>();
        Queue<Edge> pq = new PriorityQueue<>();
        boolean[] included = new boolean[NUM_NODES];
        for (Edge e : graph.get(startNode)) {
            pq.add(e);
        }
        while (!pq.isEmpty()) {
            Edge edge = pq.remove();
            if (!(included[edge.from] && included[edge.to])) {
                tree.add(edge);
                if (!included[edge.from]) {
                    included[edge.from] = true;
                    for (Edge e : graph.get(edge.from)) {
                        pq.add(e);
                    }
                }
                if (!included[edge.to]) {
                    included[edge.to] = true;
                    for (Edge e : graph.get(edge.to)) {
                        pq.add(e);
                    }
                }
            }
        }
        return tree;
    }

    public void addEdge(int from, int to, double weight) {
        graph.get(from).add(new Edge(from, to, weight));
        graph.get(to).add(new Edge(to, from, weight));
    }

    static class Edge implements Comparable<Edge>{
        int from;
        int to;
        double weight;

        public Edge(int from, int to, double weight) {
            this.from = from;
            this.to = to;
            this.weight = weight;
        }

        public int compareTo(Edge e) {
            if (e.weight < this.weight) {
                return 1;
            }
            if (e.weight > this.weight) {
                return -1;
            }
            return 0;
        }

        public String toString() {
            return "{" + to + ", " + from + ", " + weight +"}";
        }
    }
}
