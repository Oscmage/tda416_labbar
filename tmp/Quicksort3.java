import java.util.*;

public class Quicksort3 {

    public static void main(String[] args) {
        System.out.println(-101 % 5);
        int[] arr = {1,4,9,-3,6,-19,22, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE};
        // (new Quicksort3()).sort(arr);
        // for(int i : arr) {
        //     System.out.print(i + " ");
        // }
        // System.out.println();
    }
    public void sort(int[] arr) {
        sortRec(arr, 0, arr.length - 1);
    }

    public void sortRec(int[] f, int low, int high){
        int pivot = findpivot(f, low, high);
        if ( pivot != -1 ) {
            int middle = partition(f, low, high, pivot);
            sortRec (f, low, middle-1);
            sortRec (f, middle, high);
            }
    } // end quicksort    

    int findpivot (int[] f, int low, int high) {
        // check for all equal
        //(usually unlikely to happen)
        for ( int i= low; i<high; i++) {
            if ( f[i] != f[i+1] ) {
                return (low + high)/2;
            }
        }
        // they were all equal or low>=high
        return -1;
    } // end findpivot

    int partition (int[] f, int low, int high, int pivot){
        while( low < high ) {
            while (low < high && f[low] < f[pivot] ){
                low = low + 1;
            }
            while (low < high && f[high] >= f[pivot] ){
                high = high - 1;
            }
            if ( low < high ) {
                swap (f, low, high);
            }
        }
        return low;
    } // end partition

    public void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}
