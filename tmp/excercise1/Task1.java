import java.util.*;
import java.util.regex.*;
import java.io.*;
public class Task1 {
    public static void main(String[] args) {
        System.out.println(interleaveRec("Patrik", "Anna"));
        System.out.println(interleaveRec("aaaaaaaaaaaaaaaaaaaa", "bbbb"));
        System.out.println(interleaveRec("bbbb", "aaaaaaaaaaaaaaaaaaaa"));
    }

    public static String interleave(String s1, String s2) {
        char[] c1, c2;
        c1 = s1.toCharArray();
        c2 = s2.toCharArray();
        StringBuilder sb = new StringBuilder();

        int i;
        for (i = 0; i < c1.length && i < c2.length; i++) {
            sb.append(c1[i]);
            sb.append(c2[i]);
        }

        while (i < c1.length) {
            sb.append(c1[i]);
            i++;
        }
        while (i < c2.length) {
            sb.append(c2[i]);
            i++;
        }
        return sb.toString();
    }
    
    public static String interleaveRec(String s1, String s2) {
        if (s1.length() == 0) {
            return s2;
        }
        if (s2.length() == 0) {
            return s1;
        }
        return s1.substring(0,1) + s2.substring(0,1) + interleaveRec(s1.substring(1), s2.substring(1));
    }

    public static void printCount(String filename) {
        int total = 0;
        char[] characters = new char[128];
        int[] count = new int[128];
        try {
            Scanner sc = new Scanner(new File(filename));
            sc.useDelimiter("");
            while (sc.hasNext()) {
                String nextToken = sc.next();
                char next = nextToken.toCharArray()[0];
                int index = getIndex(characters, next);
                if (index == -1) {
                    index = addChar(characters, next);
                }
                count[index] += 1;
                total++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Total: " + total);
        for (int i = 0; i < characters.length && characters[i] != 0 ; i++) {
            System.out.printf("%c\t%d\t%f\n", characters[i], count[i], (1.0*count[i])/total);
        }
    }

    private static int getIndex(char[] arr, char elem) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == elem)
                return i;
        }
        return -1;
    }

    private static int addChar(char[] arr, char elem) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                arr[i] = elem;
                return i;
            }
        }
        return -1;
    }

    public static void insert(int[] arr, int elem, char index) {
        for (int i = arr.length - 1; i > index; i--) {
            arr[i] = arr[i-1];
        }
        arr[index] = elem;
        System.out.println(Arrays.toString(arr));
    }

    public static void testInsert() {
        int[] array = new int[10];
        insert(array, 5, '\0');
        insert(array, 9, 'a');
        insert(array, 1, 'b');
    }
}
