public class Binom {
    public static void main(String[] args) {
        System.out.println(binom(2, 1));
        System.out.println(binom(3, 2));
        System.out.println(binom(4, 2));
    }
    public static int binom(int n, int k) {
        if (k < 0 || n < k) {
            return 0;
        }
        if (k == 0 || n == k) {
            return 1;
        }
        return binom(n-1, k-1) + binom(n - 1, k);
    }
}
