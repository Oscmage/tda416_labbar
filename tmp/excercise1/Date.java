public class Date {

    public static void main(String[] args) {
        System.out.println((new Date(1, Month.JAN, 0)).wd);
    }
    public enum Weekday {
        MON, TUE, WED, THU, FRI, SAT, SUN;

    }
    public enum Month {
        JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC;
        public int maxDay(int year) {
            switch (this.ordinal()) {
                case 0: case 2: case 4: case 6: case 7: case 9: case 11:
                    return 31;
                case 1:
                        return isLeapYear(year) ? 29 : 28;
                default:
                        return 30;
            }
        }
    }
    int day;
    Month month;
    int year;
    Weekday wd;

    public Date(int day, Month mon, int year) {
        if (day < 1 || day > mon.maxDay(year)) throw new IllegalArgumentException("Illegal day");
        if (year < 0 || year > 99) throw new IllegalArgumentException("Illegal year");
        this.day = day;
        this.month = mon;
        this.year = year;
        this.wd = calcWd(day, mon, year);
    }

    private static Weekday calcWd(int day, Month mon, int year) {
        //1st day was saturday;
        int startday= 5;
        //begin with 1st day
        int added = 0;
        //Add years
        added += year*365;
        added += leapYearsFrom00(year);
        for (int i = 0; i < mon.ordinal(); i++) {
            added += (Month.values()[i]).maxDay(year);
        }
        added += day;
        added--;
        return Weekday.values()[added%7];

        
    }

    private static  int leapYearsFrom00(int year) {
        int ret = 0;
        for (int i = 0; i < year; i++) {
            if (isLeapYear(2000+i)) {
                ret++;
            }
        }
        return ret;
    }

    public static boolean isLeapYear(int year) {
        return year % 4 == 0;
    }
}
