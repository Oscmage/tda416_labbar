import java.util.*;
public class Random {
    public static void main(String[] args) {
        int[] results = new int[1000];
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            int res= random(0,1000);
            results[res]++;
        }
        System.out.println(Arrays.toString(results));
    }
    public static int random(int min, int max) {
        return (int)(Math.random()*(max-min) + min);
    }
}
