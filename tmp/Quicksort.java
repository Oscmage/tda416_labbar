
public class Quicksort<E extends Comparable> {

    public static void main(String[] args) {
        Integer[] arr = {1, 5, 7, 2, 3 ,34, 322, -121, 24, Integer.MAX_VALUE, Integer.MIN_VALUE};
        new Quicksort<Integer>().quicksort(arr);
        for (int i = 0; i < arr.length; i++) System.out.println(arr[i]);
    }

    public void quicksort(E[] arr) {
        quicksortRec(arr, 0, arr.length);
    }

    private void quicksortRec(final E[] arr, final int begin, final int end) {

        if (end - begin <= 0) return ;
        E pivot = arr[begin];
        int i = begin + 1;
        for (int j = i; j < end; j++) {
            if (arr[j].compareTo(pivot) < 0) {
                swap(arr, i, j);
                i++;
            }
        }
        swap(arr, begin, i - 1);
        quicksortRec(arr, begin, i - 1);
        quicksortRec(arr, i, end);
    }

    private void swap(E[] arr, int i1, int i2) {
        E tmp = arr[i1];
        arr[i1] = arr[i2];
        arr[i2] = tmp;
    }
}
