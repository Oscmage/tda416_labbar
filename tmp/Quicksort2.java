import java.util.*;

public class Quicksort2 {
    public static void main(String[] args) {
        System.out.println(-1/2);
        Integer[] arr = {1,4,9,-3,6,-19,22, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE};
        sort(arr);
        for(Integer i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
    
    public static <E extends Comparable<? super E>> void sort(E[] arr) {
        sortRec(arr, 0, arr.length - 1);
    }

    private static <E extends Comparable<? super E>> void sortRec(E[] arr, int first, int last) {
        if (last <= first) {
            return ;
        }
        int pivInd = partition(arr, first, last);
        sortRec(arr, first, pivInd - 1);
        sortRec(arr, pivInd + 1, last);
    }

    private static <E extends Comparable<? super E>> int partition(E[] arr, int first, int last) {
        E pivot = arr[first];
        int i = last;
        for (int j = i; j > first; j--) {
            if (arr[j].compareTo(pivot) > 0) {
                swap(arr, i, j);
                i--;
            }
        }
        swap(arr, first, i);
        return i;
    }

    private static <E extends Comparable<? super E>> void swap(E[] arr, int index1, int index2) {
        E tmp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = tmp;
    }
}
