public class SingleLinked {
    public static void main(String[] args) {
        SingleLinked test = new SingleLinked();
        for (int i = 0; i < 7; i++) {
            test.addChild();
        }
        test.play(3);
    }

    Node first, last;

    public void addChild() {
        if (first == null) {
            first = new Node(1);
            first.next = first;
            last = first;
        }
        else {
            last.next = new Node(last.value + 1);
            last = last.next;
            last.next = first;
        }
    }

    public void play(int order) {
        int count = 1;
        Node current = first;
        Node prev = last;
        while (current != prev) {
            if (count % order == 0) {
                System.out.println(current.value);
                prev.next = current.next;
                current = prev.next;
            } else {
                prev = prev.next;
                current = current.next;
            }
            count++;
        } // end while
        System.out.println(current.value);
        first = last = null;
    }

    class Node {
        int value;
        Node next;

        public Node(int val) {
            value = val;
        }
    }
}
