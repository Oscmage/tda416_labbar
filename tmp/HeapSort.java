import java.util.*;
public class HeapSort {

    private static int lastIndex;

    public static void main(String[] args) {
        Integer[] arr = {22, 4, 9323, 221, 42, 32 ,2};
        sort(arr);
        System.out.print(Arrays.toString(arr));
    }

    public static <E extends Comparable<? super E>> void sort(E[] arr) {
        //Make array a heap
        heapify(arr);
        E[] ret = (E[]) new Comparable[arr.length];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = remove(arr);
        }
        for (int i = 0; i < ret.length; i++) {
            arr[i] = ret[i];
        }
    }

    public static <E extends Comparable<? super E>> void heapify(E[] arr) {
        for (int i = 1; i < arr.length; i++) {
            bubbleUp(arr, i);
        }
        lastIndex = arr.length - 1;
    }

    public static <E extends Comparable<? super E>> void bubbleUp(E[] arr, int child) {
        int parent = (child - 1)/2;
        if  (arr[child].compareTo(arr[parent]) < 0 ){
            swap(arr, child, parent);
            bubbleUp(arr, parent);
        }
    }
    
    public static <E extends Comparable<? super E>> void swap(E[] arr, int i, int j) {
        E tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public static <E extends Comparable<? super E>> E remove(E[] arr) {
        E ret = arr[0];
        arr[0] = arr[lastIndex--];
        int parent = 0;
        int child1 = 1;
        int child2 = 2;
        //In each iteration, parent will have at least one child
        while (child1 <= lastIndex) {
            //Case: two children
            if (child2 <= lastIndex) {
                int least = arr[child1].compareTo(arr[child2]) < 0 ? child1 : child2;
                if (arr[parent].compareTo(arr[least]) > 0 ) {
                    swap(arr, parent, least);
                    parent = least;
                    child1 = 2 * parent + 1;
                    child2 = 2 * parent + 2;
                } else {
                    //We are in the right position
                    break;
                }
            } else {
                //Parent is not last, so it
                if (arr[parent].compareTo(arr[child1]) > 0) {
                    swap(arr, parent, child1);
                    parent = child1;
                    // We only hac one child, so we are done, there are no more elements in the heap to consider.
                }
                break;
            }
        }
        return ret;
    }
}
