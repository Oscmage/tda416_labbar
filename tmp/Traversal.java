
public class Traversal {

    public static void main(String[] args) {
        Tree<Integer> root = new Tree<>(4);
        root.add(2);
        root.add(1);
        root.add(4);
        root.add(5);
        root.add(9);
        root.add(3);
        root.add(6);
        root.add(7);
        System.out.println(root.preOrder());
        visitDepth(root, 0);
        System.out.println();
        visitDepth(root, 1);
        System.out.println();
        visitDepth(root, 2);
        System.out.println();
        visitDepth(root, 3);
        System.out.println();
        visitDepth(root, 4);
        System.out.println();
        visitDepth(root, 5);
        System.out.println();
        visitDepth(root, 30);
    }

    public static void visitDepth(Tree<Integer> t, int level) {
        if (level <= 0 || t == null) return ;
        System.out.print(" " + t.data + " ");
        visitDepth(t.left, level - 1);
        visitDepth(t.right, level - 1);
    }

    static class Tree<E extends Comparable<? super E>> {
        E data;
        Tree<E> left, right;

        public Tree (E data) {
            this.data = data;
        }

        public String preOrder() {
            return preOrderTraverse(new StringBuilder());
        }

        private String preOrderTraverse(StringBuilder sb) {
            sb.append(this.toString());
            if (left != null) {
                left.preOrderTraverse(sb);
            }
            if (right != null) {
                right.preOrderTraverse(sb);
            }
            return sb.toString();
        }

        public String inOrder() {
            return inOrderTraverse(new StringBuilder());
        }

        private String inOrderTraverse(StringBuilder sb) {
            if (left != null) {
                left.inOrderTraverse(sb);
            }
            sb.append(this.toString());
            if (right != null) {
                right.inOrderTraverse(sb);
            }
            return sb.toString();
        }

        public String postOrder() {
            return postOrderTraverse(new StringBuilder());
        }

        private String postOrderTraverse(StringBuilder sb) {
            if (left != null) {
                left.postOrderTraverse(sb);
            }
            if (right != null) {
                right.postOrderTraverse(sb);
            }
            sb.append(this.toString());
            return sb.toString();
        }

        public boolean add(E data) {
            //Value occupied
            if (this.data.equals(data)) {
                return false;
            }
            //Value is larger than current
            if (this.data.compareTo(data) < 0) {
                if (right == null) {
                    right = new Tree<>(data);
                    return true;
                }
                return right.add(data);
            }
            //Value is less than current
            if (left == null) {
                left = new Tree<>(data);
                return true;
            }
            return left.add(data);

        }

        public String toString() {
            return " " + data + " ";
        }
    } // End Tree
}
