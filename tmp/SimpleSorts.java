import java.util.*;
public class SimpleSorts {

    public static void main(String[] args) {
        Integer[] arr = {22, 36, 6, 79, 22, 45, 75, 13, 31, 62, 6};
        insertionSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public static <E extends Comparable<? super E>> void insertionSort(E[] arr) {
        for (int i = 1; i < arr.length; i++) {
            E insertVal = arr[i];
            int j;
            for (j = i; j > 0 && arr[j - 1].compareTo(insertVal) > 0; j--) {
                swap(arr, j, j - 1);
            }
            arr[j] = insertVal;
        }
    }

    public static <E extends Comparable<? super E>> void bubbleSort(E[] arr) {
        boolean isSorted;
        int last = arr.length - 1;
        do {
            isSorted = true;
            for (int i = 0; i <= last - 1; i++) {
                if (arr[i].compareTo(arr[i+1]) > 0) {
                    swap(arr, i, i+1);
                    isSorted = false;
                }
            }
            last--;
        } while(!isSorted);
    }
    
    public static <E extends Comparable<? super E>> void selectionSort(E[] arr) {
        int least;
        for (int i = 0; i < arr.length - 1; i++) {
            least = i;
            for (int j = i; j < arr.length; j++) {
                if(arr[j].compareTo(arr[least]) < 0) {
                    least = j;
                }
            }
            swap(arr, i, least);
        }
    }

    private static <T> void swap(T[] arr, int i, int j) {
        T temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

}
