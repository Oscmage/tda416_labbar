public class Check {
    public static void main(String[] args) {
        int[] arr = {2,4,6,8,10,12};
        System.out.println(arr[0] + " " + arr[3] + " " + check(arr, 10) + " ** true, summa 10");
        System.out.println(arr[0] + " " + arr[3] + " " + check(arr, 9) + " ** true, summa 10");
        System.out.println(arr[0] + " " + arr[3] + " " + check(arr, 91) + " ** true, summa 10");
        System.out.println(arr[0] + " " + arr[3] + " " + check(arr, 0) + " ** true, summa 10");
    }

    static boolean check(int[] f, int x) {
        if (f.length == 0) {
            return false;
        }
        int low = 0;
        int high = f.length - 1;
        while (low < high) {
            int sum = f[low] + f[high];
            if (sum == x) return true;
            if (sum < x) low++;
            else high--;
        }
        return false;
    }
}
