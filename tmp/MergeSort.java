import java.util.*;
public class MergeSort<E extends Comparable<? super E>> {

    public static void main(String[] args) {
        Integer[] arr = {1,4,9,-3,6,-19,22, Integer.MIN_VALUE, Integer.MAX_VALUE};
        Comparable[] ret = new MergeSort<Integer>().sort(arr);
        System.out.print(Arrays.toString(ret));
    }

    public E[] sort(E[] arr) {
        if (arr.length <= 1) return arr;
        E[] left = sort(Arrays.copyOfRange(arr, 0, arr.length/2));
        E[] right = sort(Arrays.copyOfRange(arr, arr.length/2, arr.length));
        return merge(left, right);
    }

    private E[] split(E[] arr, int first, int last) {
        if (first > last) {
            return (E[]) new Comparable[0];
        }
        E[] ret = (E[]) new Comparable[last - first + 1];
        for (int i = first, j = 0; i <= last; i++, j++) {
            ret[j] = arr[i];
        }
        return ret;
    }

    private E[] merge(E[] a, E[] b) {
        E[] ret = (E[]) new Comparable[a.length + b.length];
        int i, aP, bP;
        i = aP = bP = 0;
        while (aP < a.length && bP < b.length) {
            if (a[aP].compareTo(b[bP]) < 0) {
                ret[i] = a[aP++];
            } else {
                ret[i] = b[bP++];
            }
            i++;
        }
        while (aP < a.length) ret[i++] = a[aP++];
        while (bP < b.length) ret[i++] = b[bP++];
        return ret;
    }
}
