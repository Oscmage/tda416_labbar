
import java.util.*;

public class DirectedGraph<E extends Edge> {

    /*
     * Below code represents the graph, and should not show what kind of
     * internal represenation it uses.
     */
    private List<List<E>> adjecencyList;
    private final int noOfNodes;
    private int noOfEdges;

    public DirectedGraph(int noOfNodes) {
        this.noOfNodes = noOfNodes;
        this.noOfEdges = 0;
        adjecencyList = new ArrayList<>(noOfNodes);
        for (int i = 0; i < noOfNodes; i++) {
            adjecencyList.add(new LinkedList<>());
        }
    }

    public void addEdge(E e) {
        getEdgesFrom(e.getSource()).add(e);
        noOfEdges++;
    }

    /**
     * Returns the edge between from and to. If no such element exists, return
     * null.
     */
    //TODO: Change to returning a special edge representing NO_EDGE, with
    //infinity weight?
    private E getEdge(int from, int to) {
        for (E edge : getEdgesFrom(from)) {
            if (edge.getDest() == to) {
                return edge;
            }
        }
        return null;
    }

    /**
     * Return the list of edges from a node. This is the actual underlying list
     * of edges. Modifications to it changes the shape of the graph.
     */
    private List<E> getEdgesFrom(int from) {
        return adjecencyList.get(from);
    }

    /*
     * Code that does not depend on internal graph representation
     */

    /**
     * Uses Dijkstras algorithm to find the shortest path between two nodes.
     */
    public Iterator<E> shortestPath(int from, int to) {
        //If start and finish are the same, return an empty iterator
        if (from == to) {
            return emptyIterator();
        }
        PriorityQueue<CompDijkstraPath<E>> queue = new PriorityQueue<>();
        boolean[] visited = new boolean[noOfNodes];
        queue.add(new CompDijkstraPath<E>(from, 0.0, new LinkedList<E>()));

        // Try all paths until there are no paths left
        while (queue.size() > 0) {
            //Remove the first element from prioqueue and get the currentNode
            CompDijkstraPath<E> cdp = queue.poll();
            int currentNode = cdp.getEndpoint();
            if (!visited[currentNode]) {
                if (currentNode == to) {
                    return cdp.getPath().iterator();
                }
                visited[currentNode] = true;
                // Loop through all edges for the currentNode and look for not visited once.
                for (E edge : getEdgesFrom(currentNode)) {
                    if (!visited[edge.to]) {
                        queue.add(new CompDijkstraPath<E>(cdp, edge));
                    }
                }
            }
        }
        // If no path was found, return the empty iterator.
        return emptyIterator();
    }

    /**
     * Uses Kruskal's algorithm to find a minimum spanning tree
     * @return an interator over the MST.
     */
    public Iterator<E> minimumSpanningTree() {
        PriorityQueue<E> queue = new PriorityQueue<>(noOfEdges, new CompKruskalEdge());

        //Loop over all possible edges
        //Add them to the priorityqueue
        for (List<E> vertexList : adjecencyList) {
            for (E edge : vertexList) {
                queue.add(edge);
            }
        }

        Map<Integer, List<E>> map = new HashMap<>(noOfNodes);

        int edgeCount = 0;

        //Start with every vertix having an empty adjecency list, making each vertix a connected partition.
        for (int i = 0; i < noOfNodes; i++) {
            map.put(i, new LinkedList<>());
        }

        while (edgeCount < noOfNodes - 1) {
            // If we have not connected all vertixes, but there are nomore edges, there is no MST.
            if (queue.size() == 0) {
                return emptyIterator();
            }

            E edge = queue.poll();

            List<E> fromList = map.get(edge.getSource());
            List<E> toList = map.get(edge.getDest());

            // If the new edge extends the graph (the vertices it connects are not in the same partition
            if (fromList != toList) {
                // Merge the two lists and add the new edge
                List<E> newList, oldList;
                if (fromList.size() < toList.size()) {
                    toList.addAll(fromList);
                    oldList = fromList;
                    newList = toList;
                } else {
                    fromList.addAll(toList);
                    oldList = toList;
                    newList = fromList;
                }

                newList.add(edge);
                edgeCount++;
                for (E oldEdge : oldList) {
                    map.put(oldEdge.getSource(), newList);
                    map.put(oldEdge.getDest(), newList);
                }
                // Make both vertices point to the same list of edges, making them part of the same partition
                map.put(edge.getSource(), newList);
                map.put(edge.getDest(), newList);
            }
        }

        return map.get(0).iterator();
    }

    private Iterator<E> emptyIterator() {
        Iterator<E> emptyIterator = new Iterator<E>() {
            public boolean hasNext() { return false; }
            public E next() { return null; }
            public void remove() {}
        };
        return emptyIterator;
    }

    public static class YouDunGoofedException extends RuntimeException {}
}
