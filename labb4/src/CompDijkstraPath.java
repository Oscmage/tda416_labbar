import java.util.*;

/**
 * Immutable class for comparing paths when implementing Dijkstras algorithm.
 */
public class CompDijkstraPath<E extends Edge> implements Comparable<CompDijkstraPath<? extends Edge>> {

    private final int startNode;
    private final double cost;
    private final List<E> path;

    /**
     * @param cost The cost of travelling the path, from the start nod
     * @param path The path from the start node to the current node
     */
    public CompDijkstraPath(int startNode, double cost, List<E> path) {
        this.startNode = startNode;
        this.cost = cost;
        this.path = path;
    }

    /**
     * Create a new CompDijkstraPath that is based on the old one, but with a
     * new edge added.
     * @param old a path leading up to the newEdge
     * @param newEdge an adge to be added
     */
    public CompDijkstraPath(CompDijkstraPath<E> old, E newEdge) {
        //DEBUG
        if (old.getEndpoint() != newEdge.from) {
            throw new IllegalArgumentException("New edge does not extend the "
                    + "old path");
        }
        //END DEBUG
        this.startNode = old.startNode;
        this.cost = old.cost + newEdge.getWeight();
        this.path = new LinkedList<>(old.path);
        this.path.add(newEdge);
    }

    /**
     * Returns a copy of the path.
     */
    public List<E> getPath() {
        return new LinkedList<>(path);
    }

    public int getEndpoint() {
        if (path.size() > 0) {
            return path.get(path.size() - 1).to;
        }
        return startNode;
    }

    @Override
    public int compareTo(CompDijkstraPath other) {
        if (this.cost == other.cost) {
            return 0;
        }
        return this.cost < other.cost ? -1 : 1;
    }
}
