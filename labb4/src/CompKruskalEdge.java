import java.util.Comparator;

public class CompKruskalEdge implements Comparator<Edge> {

    public int compare(Edge one, Edge other) {
        if (one.getWeight() == other.getWeight()) {
            return 0;
        }
        return one.getWeight() < other.getWeight() ? -1 : 1;
    }
}
