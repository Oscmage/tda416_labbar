package hjort;

public class SortedLinkedListSet<E extends Comparable<? super E>> implements SimpleSet<E> {

    protected Node<E> root;
    private int size;

    @Override
    public int size() {
        return size;
    }
    
    public boolean add(E data) {
        boolean wasAdded = addHidden(data);
        if (wasAdded) {
            size++;
        }
        return wasAdded;
    }

    /**
     * Add new element to the list
     * @throws NullPointerException if data is null
     */
    private boolean addHidden(E data) { 
        if (root == null) {
            root = new Node<E>(data);
            return true;
        }
        if (areEqual(root.data, data)) {
            return false;
        }
        if (root.data.compareTo(data) > 0) {
            Node<E> newNode = new Node<>(data);
            newNode.next = root;
            root = newNode;
            return true;
        }
        return addAfter(root, data);
    }

    /** It is possible that an implementation of Comparable uses a natural 
     * ordering inconsistent with equals, which is a case we must handle
     */
    private boolean areEqual(E data1, E data2) {
        return data1.compareTo(data2) == 0 || data1.equals(data2);
    }

    /**
     * Insert a new node with given data after the given node. If the given node
     * has data that is sorted before the given data, the method guarantees
     * that the list is sorted after calling this method.
     *
     * Node or data may not
     * be null.
     */
    private boolean addAfter(Node<E> node, E data) {
        //If next node is null, we insert
        if (node.next == null) {
            node.next = new Node<E>(data);
            return true;
        }
        //If next is equal, we are not allowed to put in set
        if (areEqual(node.next.data, data)) {
            return false;
        }
        //If next node belongs after our new data, we insert the new node right
        //after the given one.
        if (node.next.data.compareTo(data) > 0) {
            Node<E> newNode = new Node<>(data);
            newNode.next = node.next;
            node.next = newNode;
            return true;
        }
        //We know our data is larger than that of the next, so we search on
        return addAfter(node.next, data);
    }

    public boolean remove(E data) {
        boolean wasRemoved = removeHidden(data);
        if (wasRemoved) {
            size--;
        }
        return wasRemoved;
    }
    /**
     * Data may not be null
     */
    private boolean removeHidden(E data) { 
        if (root == null) {
            return false; 
        }
        if (root.data.equals(data)) {
            root = root.next;
            return true;
        }
        return removeFromSublist(root, data);
    }

    /**
     * Removes data from sublist with node as root. The given node can not be removed, only its decendants.
     * @param node may not be null.
     */
    private boolean removeFromSublist(Node<E> node, E data) {
        if (node.next == null) {
            return false;
        }
        if (node.next.data.equals(data)) {
            node.next = node.next.next;
            return true;
        }
        return removeFromSublist(node.next, data);
    }
    
    public boolean contains(E data) { 
        return sublistContains(root, data);
    }

    private boolean sublistContains(Node<E> node, E data) {
        if (node == null) {
            return false;
        }
        if (node.data.equals(data)) {
            return true;
        }
        return sublistContains(node.next, data);
    }

    @Override
    public String toString() {
        Node<E> current = root;
        StringBuilder sb = new StringBuilder();
        while(current != null && current.next !=null) {
            sb.append(current);
            sb.append(" -> ");
            current = current.next;
        }
        //Current is null only if root is null
        sb.append(current);
        return sb.toString();
    }

    protected class Node<E> {
        protected E data;
        protected Node<E> next;

        /**
         * Create node with given data and no next node
         */
        protected Node (E data) {
            if (data == null) {
                throw new NullPointerException("Null data is not allowed");
            }
            this.data = data;
        }

        @Override
        public String toString() {
            return data.toString();
        }
    }
}
