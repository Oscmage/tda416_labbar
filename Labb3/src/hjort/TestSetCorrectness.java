package hjort;

import java.util.*;

public class TestSetCorrectness {

    private static final int ARGS_NEEDED = 4;
    private static final int MAX_OUTPUT = 10000;
    private static int setType, testIterations, operationsPerTest, testValuesRange;
    private static SimpleSet<Integer> testSet;
    private static Set<Integer> referenceSet;

    private static final String[] OPERATIONS = {"size", "add", "remove", "contains"};

    public static void main(String[] args) {

        //Puts values in the global variables
        parseInput(args);

        if (setType == 1) {
            System.out.println("Testing SortedLinkedListSet");
        } else {
            System.out.println("Testing SplayTreeSet");
        }
        System.out.println("===============\n");

        //Start testing
        Random rand = new Random();
        //For handling test output
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < testIterations; i++) {
               initTestingSets();
               StringBuilder operationHistory = new StringBuilder();
               for (int j = 0; j < operationsPerTest; j++) {
                   //Pick one of the four set operations
                   int operation = rand.nextInt(4);
                   int value = rand.nextInt(testValuesRange);
                   boolean testSuccessful = performOperation(operation, value);
                   //If a test was unsuccesful, we report it and start a new test iteration
                   operationHistory.append(OPERATIONS[operation] + " with value " + value + "\n" + testSet);
                   if (!testSuccessful) {
                       sb.append("Test #" + j + " failed:\n" + operationHistory.toString() + "\n\n\n");
                       break;
                   }
               }
        }

        //Report results
        String output;
        if (sb.length() > MAX_OUTPUT) {
            output = "Too much output\n\n" + sb.substring(0, MAX_OUTPUT) + "...\n";
        } else {
            output = sb.toString();
        }
        System.err.print(output);
        System.out.println("Tests completed.\n---------------\n");
    }

    /**
     * Parse input from args and put in static variables.
     * After the method returns, the global integer values of setType,
     * testIterations, operationsPerTest, testValuesRange are guaranteed to be 
     * set, if the input was valid. If it was not valid, the program will have 
     * exited.
     */
    private static void parseInput(String[] args) {
        //Input validation
        if (args.length < ARGS_NEEDED) {
            System.err.println("Needs at least " + ARGS_NEEDED + " arguments");
            System.exit(1);
        }

        //Array needed temporary to put the arguments in
        int parsedArgs[] = new int[ARGS_NEEDED];
        try {
            for (int i = 0; i < ARGS_NEEDED; i++) {
                parsedArgs[i] = Integer.parseInt(args[i]);
            }
        } catch (NumberFormatException e) {
            System.err.println(
                    "This program only takes integer arguements. See Lab PM for more info");
            e.printStackTrace();
            System.exit(2);
        }

        //Set the global values, making sure they have valid values as we go
        setType = parsedArgs[0];
        if (setType != 1 && setType != 2) System.exit(3);
        testIterations = parsedArgs[1];
        if (testIterations < 0) System.exit(3);
        operationsPerTest = parsedArgs[2];
        if (operationsPerTest < 0) System.exit(3);
        testValuesRange = parsedArgs[3];
        if (testValuesRange < 1) System.exit(3);
    }

    private static void initTestingSets() {
        if (setType == 1) {
            testSet = new SortedLinkedListSet<>();
        } else if (setType == 2) {
            testSet = new SplayTreeSet<>();
        } else {
            System.err.println("First argument shoudl be 1 or 2");
        }

        referenceSet = new HashSet<>();
    }
    
    /**
     * Performs one of the operations from the tested interface on both the test
     * set and the reference set, and reports back whether they gave the same
     * results
     */
    private static boolean performOperation(int operation, int value) {
        if (operation == 0) {
            return testSet.size() == referenceSet.size();
        } else if (operation == 1) {
            return testSet.add(value) == referenceSet.add(value);
        } else if (operation == 2) {
            return testSet.remove(value) == referenceSet.remove(value);
        } else if (operation == 3) {
            return testSet.contains(value) == referenceSet.contains(value);
        }
        throw new IllegalArgumentException(
                "Operation must have a value from 0 to 3");
    }

}
