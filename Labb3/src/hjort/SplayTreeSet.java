package hjort;

public class SplayTreeSet<E extends Comparable<? super E>> implements SimpleSet<E> {

    private int size;
    private Node<E> root;

    /**
     * Splay the entire tree to bring the given node to the top.
     */
    private void splay(Node<E> node) {
        if (node == null || node == root) {
            return ;
        }
        //Zig case: we are child of the root
        if (node.parent == root) {
            //Zig
            if (node == root.left) {
                rotateRight(root);
            } else {
                rotateLeft(root);
            }

        } else if (node == node.parent.left) {
            //If we are not a child of the root, we have at least one parent
            if (node.parent == node.parent.parent.left) {
                //Zig-zig
                rotateRight(node.parent.parent);
                rotateRight(node.parent);
            } else {
                //Zig-zag
                rotateRight(node.parent);
                rotateLeft(node.parent);
            }
        } else if (node.parent == node.parent.parent.left) {
        //We are a right child
            //Zig-zag
            rotateLeft(node.parent);
            rotateRight(node.parent);
        } else {
            //Zig-zig
            rotateLeft(node.parent.parent);
            rotateLeft(node.parent);
        }
        splay(node);
    }

    /**
     * Assumes that node has a right child.
     */
    private void rotateLeft(final Node<E> node) {
        final Node<E> right = node.right;
        final Node<E> parent = node.parent;
        if (node != root) {
            if (node == parent.left) {
                parent.left = right;
            } else {
                parent.right = right;
            }
        } else {
            root = node.right;
        }
        node.right = right.left;
        if (right.left != null) {
            right.left.parent = node;
        }
        right.left = node;
        right.parent = parent;

        node.parent = right;
    }

    /**
     * Assumes that node has a left child.
     */
    private void rotateRight(final Node<E> node) {
        final Node<E> left = node.left;
        final Node<E> parent = node.parent;
        if (node != root) {
            if (node == parent.right) {
                parent.right = left;
            } else {
                parent.left = left;
            }
        } else {
            root = node.left;
        }
        node.left = left.right;
        if (left.right != null) {
            left.right.parent = node;
        }
        left.right = node;
        left.parent = parent;

        node.parent = left;
    }

    @Override
    public int size() { 
        return size; 
    }

    /**
     * Wrapper for handling incrementation when adding
     */
    @Override
    public boolean add(E data) {
        boolean wasAdded = addWrapped(data);
        if (wasAdded) {
            size++;
        }
        return wasAdded;
    }

    /**
     * Tries to add a node to the tree and returns true if it is succesful, and 
     * false otherwise. No matter the outcome, the tree is splayed.
     */
    private boolean addWrapped(E data) {
        if (root == null) {
            //Root has no parent, therefore null
            root = new Node<>(data, null);
            return true;
        }
        return addToSubtree(root, data);
    }
    
    /**
     * Recursive method for adding. No paramter may be null, or exceptions will be
     * thrown.
     */
    private boolean addToSubtree(Node<E> node, E data) {
        if (areEqual(node.data, data)) {
            return false;
        }
        int comp = data.compareTo(node.data);
        if (comp < 0) {
            if (node.left == null) {
                //We found a place to insert
                node.left = new Node<>(data, node);
                splay(node.left);
                return true;
            } 
            //Node has a left child, and data belongs left of the node.
            return addToSubtree(node.left, data);
        } 
        //Only possibility left is that we belong to the right
        if (node.right == null) {
            node.right = new Node<>(data, node);
            splay(node.right);
            return true;
        } 
        return addToSubtree(node.right, data);
    }

    /** It is possible that an implementation of Comparable uses a natural 
     * ordering inconsistent with equals, which is a case we must handle
     */
    private boolean areEqual(E data1, E data2) {
        return data1.compareTo(data2) == 0 || data1.equals(data2);
    }

    /**
     * Wrapper for decrementing when deleting
     */
    @Override
    public boolean remove(E data) { 
        boolean wasRemoved = removeWrapped(data);
        if (wasRemoved) {
            size--;
        }
        return wasRemoved; 
    }

    /**
     * Remove node with data from the tree. If successful, return true, otherwise
     * return false. No matter the result the tree is splayed.
     */
    private boolean removeWrapped(E data) {
        if (root == null) {
            return false;
        }
        return removeFromSubtree(root, data);
    }

    /**
     * Recursively removes the given data from the subtree starting with node.
     */
    private boolean removeFromSubtree(Node<E> node, E data) {
        if (areEqual(node.data, data)) {
            Node<E> replacement = findReplacementNode(node);
            if (replacement != null) {
                //We are not a leaf: replace with something else
                node.data = replacement.data;
                //Kill the leaf with copied
                killChildedNode(replacement);
                splay(node);
            } else {
                Node<E> parent = node.parent;
                killNode(node);
                splay(parent);
            }
            return true;
        }
        int comp = data.compareTo(node.data);
        if (comp < 0) {
            if (node.left == null) {
                splay(node);
                return false;
            }
            return removeFromSubtree(node.left, data);
        }
        if (node.right == null) {
            splay(node);
            return false;
        }
        return removeFromSubtree(node.right, data);
    }

    /**
     * Removes a node from the tree. May only remove a node with one or no children. This method assumes the tree is in a consistent state, meaning it is
     * a Binary Search Tree, that all its pointers are consistent (parents and 
     * children point to each other) and that the given node is not null.
     */
    private void killNode(Node<E> node) {
        if (node == root) {
            root = null;
            return;
        }
        if (node.parent.left == node) {
            node.parent.left = null;
        } else if (node.parent.right == node) {
            node.parent.right = null;
        } else {
            throw new Error("Child not present in parent");
        }
    }

    private void killChildedNode(Node<E> node) {
        Node<E> moveUp;
        if (node.left == null) {
            moveUp = node.right;
        } else {
            moveUp = node.left;
        }

        if (node == root) {
            root = moveUp;
        } else {
            if (node.parent.left == node) {
                node.parent.left = moveUp;
            } else {
                node.parent.right = moveUp;
            }
        }
        if (moveUp != null) {
            moveUp.parent = node.parent;
        }
    }

    /**
     * Helper method for removing nodes. Finds the node to replace a node to be
     * removed with.
     */
    private Node<E> findReplacementNode(Node<E> node) {
        if (node.left == null && node.right == null) {
            return null;
        }
        if (node.left != null) {
            return findLargest(node.left);
        }
        return findSmallest(node.right);
    }

    /**
     * Find largest node in subtree. Node may not be null.
     */
    private Node<E> findLargest(Node<E> node) {
        if (node.right == null) {
            return node;
        }
        return findLargest(node.right);
    }

    /**
     * Find smallest node in subtree. Node may not be null.
     */
    private Node<E> findSmallest(Node<E> node) {
        if (node.left == null) {
            return node;
        }
        return findSmallest(node.left);
    }

    @Override
    public boolean contains(E data) { 
        if (root == null) {
            return false;
        }
        return contains(root, data);
    }

    private boolean contains(Node<E> node, E data) {
        if (areEqual(node.data, data)) {
            splay(node);
            return true;
        }
        int comp = data.compareTo(node.data);
        if (comp < 0) {
            if (node.left == null) {
                splay(node);
                return false;
            }
            return contains(node.left, data);
        }
        if (node.right == null) {
            splay(node);
            return false;
        }
        return contains(node.right, data);
    }


    //TODO:
    /**
     * Gives the node with given data or, if none exists, the last visited node
     * in the search, which would be its parent if it was inserted.
     *
     * Helper method for add and remove, to reduce code
     */
    private Node<E> find(E data) {
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (root != null) {
            root.preorderTraverse(0, sb);
            return sb.toString();
        }
        return "Empty tree\n";
    }

    protected class Node<E> {
        public E data;
        public Node<E> left, right, parent;

        public Node (E data, Node<E> parent) {
            this.data = data;
            this.parent = parent;
        }

        protected void preorderTraverse(int depth, StringBuilder sb) {
            String spaces = "";
            for (int i = 0; i < depth; i++) {
                spaces += " ";
            }
            sb.append(spaces + this + "\n");
            if (left != null) {
                left.preorderTraverse(depth + 1, sb);
            } else {
                sb.append(spaces + " " + "null\n");
            }
            if (right != null) {
                right.preorderTraverse(depth + 1, sb);
            } else {
                sb.append(spaces + " " + "null\n");
            }
        }

        @Override
        public String toString() {
            E parentData = parent == null ? null : parent.data;
            return "" + data + " (parent: " + parentData + ")";
        }
    }
}
