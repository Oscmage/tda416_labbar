\documentclass{article}
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{ {img/} }
\usepackage{caption}

\title{Lab 3}
\date{2016-02-19}
\author{Oscar Evertsson \& Rikard Hjort}

\begin{document}
\pagenumbering{arabic}
\maketitle

\section{Complexity for \textit{SortedLinkedListSet}}

    \subsection{contains} \label{LinkedContains}

    In the worst case we need to go through the whole linked list to find the
    element we are looking for. This is because, being a single linked list, it
    can only be traversed in one direction, visiting one node at a time, and
    not passing over any nodes. Since the node we are looking for might be in
    the last place of the list, or larger than any element in the list, we
    might need to traverse the whole list to decide if the element is present.
    This means that the \textit{contains}-operation has a worst case complexity
    of $O(n)$.

    \subsection{add}

    When performing an \textit{add}-operation, we must first go through the
    same steps as in the \textit{contains}-operation - that is, finding the
    proper place for the element, which means traversing the list until we come
    upon a place where it could already be. If it is there, we return false,
    and otherwise we insert it. Thus, add performs the same steps as contains,
    and an additional operation if the element is added (creating a new node
    and changing the links in the list so that it is inserted). Since this
    final step takes constant time, the \textit{add}-operation has the same
    complexity as \textit{contains}, which is $O(n)$. 

    \subsection{remove}

    By the same token as above, the remove operation has a complexity of
    $O(n)$. This is because we do the same thing as in \textit{add}, which is
    to locate the element or the place where it should be. If the element is
    found it is removed, which just means changing a constant number of
    pointers, which takes constant time.

    \begin{figure}

        \centering

        \includegraphics[width=\textwidth]{Linear}

        \captionsetup{justification=centering}

        \caption{The average operation speed on a \textit{SortedLinkedListSet}
        plotted against the size of that set, together with a function $f(n)
        \in O(n).$}

    \end{figure}

    \subsection{Fit of function to data points} The fit of the function to the
    data is quite good, which is to be expected: a linked list is a fairly
    straight-forward and predictible data structure, and the average time of
    operations with random data will be fairly deterministic. The average
    position of a randomly selected value will be in the middle of the list, so
    on average wwe just traverse the list $n/2$ times.

\section{Complexity for \textit{SplayTreeSet}}

    \subsection{contains}

    The worst possible scenario for any search operation on a binary search
    tree (BST) is when the BST is essentially a linked list: every node has
    exactly one child. For a SplayTree, this situation is possible, for
    instance when a set of elements have been added to the tree in a sorted
    order. In that case, the \textit{contains}-operation has complexity $O(n)$
    by the same token as for the \textit{contains}-operation for the linked
    list, discussed in section \ref{LinkedContains}.

    \subsubsection{Amortized complexity and splaying}

    However, if a \textit{contains}-operation has been performed with the worst
    case complexity, that means that after the operation returns, the tree has
    been splayed on the farthest node. That means that the splaying has passed
    every node in the tree. This improves the efficiency of the following
    operations. More on this in section \ref{SplayFit} 

    \subsection{add}

    Just as in the \textit{contains}-operation we could possibly have to go
    through the whole tree to find the place to put the new node, followed by
    splaying it to the top. This gives us a complexity of $O(2n) = O(n)$. The
    only difference from the \textit{contains}-operation is the constant
    operations of adding the new element which are negligible in our "hand
    waving" analysis.

    \subsection{remove}
    
    Like previously the complexity for the \textit{remove}-operation follows
    the pattern of the \textit{add}-operation. The distinction is the constant
    operation of removing rather than adding. This results in a complexity of
    $O(n)$. 

    \begin{figure}

        \includegraphics[width=\textwidth]{Log}

        \captionsetup{justification=centering}

        \caption{The average operation speed on a \textit{SplayTreeSet} plotted
        against the size of that set, together with a function $f(n) \in O(\log
        n).$}

    \end{figure}

    \subsection{Fit of function to data points}  \label{SplayFit}

    As stated above, the worst case complexity of any single given operation is
    in $O(n)$. However, it's important to note that this only holds for single
    operations. For a series of operations, the Splay Tree most often gives an
    average operation time in $O(\log n)$. This is due to the splaying
    perfomred after each operation.

    According to the original paper on Splay Trees, "Splaying not only moves x
    to the root, but roughly halves the depth of every node along the access
    path." \cite[p. 656]{SplayTrees}. Thus a tree accessed many times will be
    fairly balanced after those operations, since the depth of most nodes have
    been halved at least at some point.

    We also note that the data indicates that we have a better and better fit
    for a $\log_2 n$ curve as the size of the set increases. This seems
    sensible, since small trees are more likely to have strong imbalances than
    large trees, since the splaying most often improves balance, and the law of
    large number predicts that the average depth of nodes should be closer the
    expected average detph of $\log_2 n$ as $n$ increases.

\bibliography{Labb3} 

\bibliographystyle{ieeetr}

\end{document}
