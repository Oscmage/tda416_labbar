import java.lang.Double;
import java.util.ArrayList;
import java.util.List;

public class Lab2a {

    public static double[] simplifyShape(double[] poly, int k) {
        int totPairs = poly.length / 2;

        List<Point> points = new ArrayList<>(totPairs);
        //Add every pair as a point in the points ArrayList.
        for (int i = 0; i < totPairs; i++) {
            points.add(new Point(poly[i * 2], poly[i * 2 + 1]));
        }

        print(points);

        //Make sure we remove so it's only k pairs left
        for (int count = totPairs - k; count > 0; count--) {

            double minValue = Double.MAX_VALUE;
            int minPos = 1;
            double currentValue;
            //Get position of least valued point
            //Exclude first and last element from calculations
            for (int i = 1; i < points.size() - 1; i++) {
                //Gets the value measurement for each point P
                currentValue = calcValue(points.get(i), points.get(i - 1), points.get(i + 1));
                if (currentValue < minValue) {
                    minValue = currentValue;
                    minPos = i;
                }
            }
            points.remove(minPos);
        }

        print(points);

        double[] ret = new double[k * 2];
        int retPos = 0;

        for (Point p : points) {
            ret[retPos] = p.getX();
            ret[retPos + 1] = p.getY();
            retPos += 2;
        }

        return ret;
    }

    private static void print(List<Point> list) {
        System.out.println("Size: " + list.size());
        for (Point p : list) {
            System.out.println(p);
        }
        System.out.println("\n");
    }

    /**
     * Calculates the "value measurement"("värdemåttet") for the point P.
     * @param middle the point "P"
     * @param left the point left of P
     * @param right the point right of P
     * @return the value measurement for the point middle
     */
    private static double calcValue(Point middle, Point left, Point right) {
        return middle.getDistance(left) + middle.getDistance(right) - left.getDistance(right);
    }


}
