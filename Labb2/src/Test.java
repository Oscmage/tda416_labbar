public class Test {

    public static void main(String[] args) {
        DLList<String> list = new DLList<>();
        list.addFirst("Hello");
        list.addFirst("World");
        list.addLast("!");
        list.addLast("!");
        print(list);
        printBack(list);
        DLList<String>.Node n = list.addFirst("Test node:");
        list.insertBefore("Before", n);
        list.insertAfter("After", n);
        print(list);
        printBack(list);
        list.remove(n);
        print(list);
        printBack(list);
    }

    static void printBack(DLList list) {
        DLList.Node last = list.getFirst();
        DLList.Node curr = list.getLast();
        do {
            System.out.print(curr.elt);
            System.out.print(" <-> ");
            curr = curr.getPrev();
        }
        while (last != curr);

        System.out.println(curr.elt);

    }

    static void print(DLList list) {
        DLList.Node last = list.getLast();
        DLList.Node curr = list.getFirst();
        do {
            System.out.print(curr.elt);
            System.out.print(" <-> ");
            curr = curr.getNext();
        }
        while (last != curr);

        System.out.println(curr.elt);
    }

}
