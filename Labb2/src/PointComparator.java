import java.util.Comparator;

public class PointComparator implements Comparator<DLList<Point>.Node> {
	

    /**
     * Can only compare nodes that have two neighbours
     */
	public int compare(DLList<Point>.Node n1, DLList<Point>.Node n2) {
		Point n1Prev = n1.getPrev().elt;
		Point n1Next = n1.getNext().elt;
		double n1Value = n1.elt.calcValue(n1Prev, n1Next);

		Point n2Prev = n2.getPrev().elt;
		Point n2Next = n2.getNext().elt;
		double n2Value = n2.elt.calcValue(n2Prev, n2Next);

		double result = n1Value - n2Value;

		if (result < 0) return -1;
		if (result > 0) return 1;
		return 0;
	}
}