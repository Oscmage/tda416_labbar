import java.util.PriorityQueue;

public class Lab2b {
    public static double[] simplifyShape(double[] poly, int k) {

        //Create a list of the points, and a priority queue for the nodes in
        //that list.
        int nbrOfPoints = poly.length/2;
        DLList<Point> list = Point.toPointArray(poly);

        PriorityQueue<DLList<Point>.Node> queue = new PriorityQueue<>(nbrOfPoints, new PointComparator());

        //We put all nodes in the queue, making sure that we do not send in the first and last nodes as we do so, since they must always be maintained
        for (DLList<Point>.Node node = list.getFirst().getNext(); node != list.getLast(); node = node.getNext()) {
            queue.add(node);
        }

        //Perform the loop n - k times, giving us k remaining elements
        for (int i = 0; i < nbrOfPoints - k; i++) {

            //Remove the least important point, but remember its neighbours.
            DLList<Point>.Node point = queue.poll();
            DLList<Point>.Node prev = point.getPrev();
            DLList<Point>.Node next = point.getNext();
            list.remove(point);

            //Update old points
            if (prev != list.getFirst()) {
                queue.remove(prev);
                queue.add(prev);
            }
            if (next != list.getLast()) {
                queue.remove(next);
                queue.add(next);
            }
        }

        double[] ret = new double[k*2];

        int i = 0;
        for (DLList<Point>.Node current = list.getFirst(); current != null; current = current.getNext()) {
            ret[i] = current.elt.getX();
            ret[i+1] = current.elt.getY();
            i = i + 2;
        }

		return ret;
    }
}
