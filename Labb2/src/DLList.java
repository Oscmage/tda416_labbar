/** Doubly-linked list with user access to nodes
*/
public class DLList<E> {
    public class Node {
        /** The contents of the node is public */
        public E elt;

        protected Node prev, next;

        Node() {
            this(null);
        }
        Node(E elt) {
            this.elt = elt;
            prev = next = null;
        }

        public Node getNext() {
            return next;
        }

        public Node getPrev() {
            return prev;
        }
    }

    /** first and last nodes in list, null when list is empty */
    Node first, last;

    DLList() {
        first = last = null;
    }

    /** inserts an element at the beginning of the list
     * @param e   the new element value
     * @return    the node holding the added element
     */
    public Node addFirst(E e) {
        Node newNode = new Node(e);

        if (first != null) {
            Node oldFirst = first;
            oldFirst.prev = newNode;
            newNode.next = oldFirst;
        } else {
            last = newNode;
        }
        first = newNode;

        return newNode;
    }

    /** inserts an element at then end of the list
     * @param e   the new element
     * @return    the node holding the added element
     */
    public Node addLast(E e) {
        Node newNode = new Node(e);

        if (last != null) {
            Node oldLast = last;
            oldLast.next = newNode;
            newNode.prev = oldLast;
        } else {
            first = newNode;
        }
        last = newNode;

        return newNode;
    }

    /**
     * @return the node of the list's first element, null if list is empty
     */
    public Node getFirst() {
        return first;
    }

    /**
     * @return    the node of the list's last element, null if list is empty
     */
    public Node getLast() {
        return last;
    }

    /** inserts a new element after a specified node
     * @param e   the new element
     * @param node   the node after which to insert the element, must be non-null and a node belonging to this list
     * @return    the node holding the inserted element
     */
    public Node insertAfter(E e, Node node) {
        Node nextNode = node.next;
        Node newNode = new Node(e);

        node.next = newNode;
        newNode.prev = node;
        newNode.next = nextNode;

        if (nextNode != null) {
            nextNode.prev = newNode;
        } else {
            last = newNode;
        }

        return newNode;
    }

    /** inserts a new element before a specified node
     * @param e   the new element
     * @param node   the node before which to insert the element, must be non-null and a node belonging to this list
     * @return    the node holding the inserted element
     */
    public Node insertBefore(E e, Node node) {
        Node newNode = new Node(e);
        Node previousNode = node.prev;

        newNode.prev = previousNode;
        newNode.next = node;
        node.prev = newNode;

        if (previousNode != null) {
            previousNode.next = newNode;
        } else {
            first = newNode;
        }

        return newNode;
    }

    /** removes an element
     * @param node   then node containing the element that will be removed, must be non-null and a node belonging to this list
     */
    public void remove(Node node) {
        Node nextNode = node.next;
        Node previousNode = node.prev;

        if (previousNode != null) {
            previousNode.next = nextNode;
            if (nextNode != null) {
                nextNode.prev = previousNode;
            } else {
                last = previousNode;
            }
        } else {
            nextNode.prev = null;
            first = nextNode;
        }
    }
}
