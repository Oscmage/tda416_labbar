/**
 * This class represents a point in the 2d plane, with a x and y coordinate.
 */
class Point {
    private double x, y;

    public Point (double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    /**
     * Calculates the distance between two points using Pythagoras.
     * @param p the point to compare distance from.
     * @return the distance between the points.
     */
    public double getDistance(Point p) {
        //Pythagoras
        return Math.sqrt(Math.pow(x - p.x, 2) + Math.pow(y - p.y, 2));

    }

    @Override
    public String toString() {
        return "Point (" + x + "," + y + ")";
    }

        /**
     * Calculates the "value measurement"("värdemåttet") for the point P.
     * @param left the point left of P
     * @param right the point right of P
     * @return the value measurement for the point middle
     */
    public double calcValue(Point left, Point right) {
        return this.getDistance(left) + this.getDistance(right) - left.getDistance(right);
    }


    public static DLList<Point> toPointArray(double[] arr) {
        DLList<Point> ret = new DLList<>();
        for (int i = 0; i < arr.length; i += 2) {
            ret.addLast(new Point(arr[i], arr[i+1]));
        }
        return ret;
    }
}
